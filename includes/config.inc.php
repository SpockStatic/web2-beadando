<?php
$ablakcim = array(
    'cim' => 'VadmadárKorház.',

);

$fejlec = array(
    'kepforras' => 'log01.png',
    'kepalt' => 'logo',
	'cim' => 'VadmadárKorház',
	'motto' => ''
);

$wall = array(
    'kepforras' => 'wall01.jpg',
    'kepalt' => 'wall',
    
);

$lablec = array(
    
    'ceg'=>'Cím: Sukoró, Ivókút u.14.Székesfehérvár,Takarodó u.5</br>
    		E-mail: vadmadarkorhaz@gmail.com</br>
			Telefon: +36 30 992 4876'
);

$oldalak = array(
	'/' => array('fajl' => 'cimlap', 'szoveg' => 'Főoldal', 'menun' => array(1,1)),
	'korhaz' => array('fajl' => 'korhaz', 'szoveg' => 'Korház', 'menun' => array(1,1)),
	'kapcsolat' => array('fajl' => 'kapcsolat', 'szoveg' => 'A mi híreink', 'menun' => array(0,1)),
	'kapcsolat2' => array('fajl' => 'kapcsolat2', 'szoveg' => 'A ti híreitek', 'menun' => array(0,1)),
	'kapcsolat3' => array('fajl' => 'kapcsolat3', 'szoveg' => 'Kommenteitek', 'menun' => array(0,1)),
	'video' => array('fajl' => 'video', 'szoveg' => 'Video', 'menun' => array(1,1)),
    'kepek' => array('fajl' => 'kepek', 'szoveg' => 'Képgaléria', 'menun' => array(1,1)),
	'belepes' => array('fajl' => 'belepes', 'szoveg' => 'Belépés', 'menun' => array(1,0)),
    'kilepes' => array('fajl' => 'kilepes', 'szoveg' => 'Kilépés', 'menun' => array(0,1)),
	'admin' => array('fajl' => 'admin', 'szoveg' => 'Admin', 'menun' => array(1,1)),
    'belep' => array('fajl' => 'belep', 'szoveg' => '', 'menun' => array(0,0)),
    'regisztral' => array('fajl' => 'regisztral', 'szoveg' => '', 'menun' => array(0,0))
	
);


$hiba_oldal = array ('fajl' => '404', 'szoveg' => 'A keresett oldal nem található!');
?>

