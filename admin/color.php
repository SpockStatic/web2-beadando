<?php
    $result = Array("color"=>"", "bgcolor" => "", "text" => "");
    $result["color"] = "rgb(".rand(0,255).",".rand(0,255).",".rand(0,255).")";
    $result["bgcolor"] = "rgb(".rand(0,255).",".rand(0,255).",".rand(0,255).")";
    switch($_GET["nyelv"]) {
        case "nemet":
            $result["text"] = " Ein Systemadministrator (von lateinisch administrare „verwalten“; auch Administrator oder (formlos) Admin, Sysadmin, Netzwerkadministrator, Netzwerkverwalter, Netzadministrator, Netzverwalter, Systemmanager, Systemverwalter, Systembetreuer, Systemoperator, Operator oder Sysop (aus „Systemoperator“) genannt) verwaltet Computersysteme auf der Basis umfassender Zugriffsrechte auf das System.

		Systemadministratoren planen, installieren, konfigurieren und pflegen die informationstechnische Infrastruktur (IT-Infrastruktur) eines Unternehmens oder anderer Organisationen. Als Operatoren führen sie die zum laufenden Betrieb der Computeranlagen erforderlichen manuellen Tätigkeiten aus.

		Die IT-Infrastruktur bestimmt in vielen Unternehmen, Organisationen u. Ä. in einem solchen Maße den Ablauf der Geschäftsprozesse, dass sie ein geschäftskritischer Faktor geworden ist. Aus diesem Grunde kommt dem reibungslosen Funktionieren dieser Infrastruktur ein erhebliches Gewicht zu. ";
            break;
        case "angol":
            $result["text"] = " A system administrator, or sysadmin, is a person who is responsible for the upkeep, configuration, and reliable operation of computer systems; especially multi-user computers, such as servers. The system administrator seeks to ensure that the uptime, performance, resources, and security of the computers they manage meet the needs of the users, without exceeding a set budget when doing so.

To meet these needs, a system administrator may acquire, install, or upgrade computer components and software; provide routine automation; maintain security policies; troubleshoot; train or supervise staff; or offer technical support for projects. ";
            break;
    }
    echo json_encode($result);
?>