<?php

  include "felhasznalo.class.php";

  echo "Teszt1:<br><br>";
  $teszt1 = new Hallgato("Kis Pál", "6000 Kecskemét Vacsi 1", "PAE00001",
                         "pali", "1998-10-03", "Info");
  $teszt1->jegybeir(3);
  $teszt1->jegybeir(5);
  $teszt1->jegybeir(4);
  //$teszt1->kiir();
  kiir($teszt1);
  
  echo "<br><br>Admin teszt:<br><br>";
  $teszt2 = new Admin("Kiss Mária", "6060 Szabadszállás", "PAE00002",
                      "maria", "1980-01-02", 10, "Adminisztrátor");
  kiir($teszt2);
  $teszt2->fizetesemeles(5);
  $teszt2->feladatmodositas("Megbízható admin");
  echo "<br>";
  kiir($teszt2);

  echo "<br><br>Pártfogó teszt:<br><br>";
  $teszt3 = new Tanar("Nagy Béla", "1000 Baja Nagy Tér 3", "PAE00003",
                      "bela", "1968-03-01", 35, "Info");
  kiir($teszt3);
  $teszt3->tantargyfelvesz("Info I", 125);
  $teszt3->tantargyfelvesz("Info II", 88);
  $teszt3->tantargyfelvesz("Info", 111);
  kiir($teszt3);
  $teszt3->tantargylevesz("Info II");
  kiir($teszt3);
  
?>
