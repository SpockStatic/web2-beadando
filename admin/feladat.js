$(document).ready(function() {
    $("div").css({
        "border": "2px solid black",
        "border-radius": "15px",
        "padding": "12px",
        "background": "#ddp"
    });
    $("#angol").hide();
    $("#bnemet").click(function() {
        $("#angol").hide();
        $("#nemet").show();
    });
    $("#bangol").click(function() {
        $("#nemet").hide();
        $("#angol").show();
    });
    $("#fordits").click(function() {
        $("div").toggle();
    });
    $("#modosits").click(function(e) {
      var valasztas = $("#nyelv").val();
      if (valasztas != "") {
        $.getJSON(
          "color.php",
          {"nyelv": valasztas},
          function(data) {
              $("#"+valasztas).css({
                  "color": data.color,
                  "background": data.bgcolor,
              }).html(data.text);
          }
        )
      };
    });    
})