

  <a id="korhaz" href="http://vadmadarkorhaz.hu/fooldal" target="_blank"> A Vadmadárkórház célja: </a>
    <article>
    <p>A vadon élő, sérült madarak gyógyítása, azt követően - ha lehetséges- visszaengedésük a szabad természetbe.
     Maradandó károsodás esetén élethosszan tartó gondozásuk, oktatás, nevelés, szemlélet formálás, ismeretterjesztés, a természet védelme.</p>

    <p><img id="bird1" src="./images/bird1.jpg"></p><img id="bird2" src="./images/bird2.jpg">A madarak gyógyítását nem azért végezzük, mert azt hisszük, hogy az általunk meggyógyított néhány madár megmenthet egy- egy fajt a kipusztulástól. <p></br>A sérült madár gyógyítása emberi létünkből fakadó belső késztetés. Az ember késztetése, aki nem tudja elnézni a beteg madár szenvedését, aki nem tud elmenni az út szélén fekvő, elütött, sérült madár mellett, aki nem tudja otthagyni a megégettet a villanyvezeték alatt, a mérgezettet görcsök között elpusztulni, és aki talán felelősséget is érez mindazért a gyötrelemért, melynek okozója maga az ember.</p></br>
    <p>Azoknak szeretnénk segíteni a madarak gyógyításával, akiknek a lelkiismeretét furdalja, akiknek a lelkét nyomasztja a madár szenvedése, akik segíteni akarnak, és ezért megoldást keresnek a gyógyításukra. Ezért hisszük, hogy a madármentés lélekmentés is.
 </p>
</article>
<article id="cikk">
 <p><img id="favicon" src="./images/favicon.ico">Nem teszünk különbséget a madarak között. Minden madarat fogadunk. Jó példát szeretnénk mutatni állatszeretből, segítőkészségből, emberségből és felelősségvállalásból.</p>
 <p><img id="favicon" src="./images/favicon.ico">Hiszünk abban, hogy tevékenységünk hat a minket megismerő emberek gondolkodására, a magunk módján hozzájárulunk, hogy világunk kicsit jobbá váljon: Hiszünk abban, hogy a madármentés egyben lélekmentés is!</p> 
<p><img id="favicon" src="./images/favicon.ico">Az utánunk következő genereció olyan lesz, amilyenné mi neveljük- általunk mutatott példánkon keresztül- őket. Ezzel pedig mi magunk meghatározzuk gyerekeink jövőjét, sorsát.</p> 
<p><img id="favicon" src="./images/favicon.ico">A madármentés áldozatos, kitartást, lemondást, segítőkészséget, együttérzést igénylő tevékenység. A madármentés egy másfajta szemléletet, mintát mutat a gyerekeknek. Ezért gondoljuk, hogy a madármentés egyben lélekmentés is. Előadásokon, kötetlen beszélgetések keretében, a madarak példáján keresztül foglalkozunk a segítőkészség, tolerancia, szolidaritás, egymás iránti tisztelet kérdéseivel.</p> 
<p><img id="favicon" src="./images/favicon.ico">Az, aki lehajol egy fészkéből kiesett verébfiókáért, aki behoz egy szárnyon lőtt egerészölyvet, az talán segíteni fog embertársának is.
Előadásokat, foglalkozásokat tartunk óvodáknak, iskoláknak, felnőtt csoportoknak, cégeknek, madárismeretről, madármentésről, élő madárbemutatóval.</p></article> 

<br/>
<h1>Egy kis szórakozás:</h1>
<br/>
<a href="templates/pages/objsub.html"><button>Madár kép átméretező</button></a>
<a href="templates/pages/madar.php"><button>Kedvenc madarak</button></a>

