<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
  <title>Madár Sorrend</title>
  <script type="text/javascript" src="jquery.min.js"></script>
  <script type="text/javascript" src="jquery-ui.min.js"></script>
  
  <style type="text/css">
    body {
      font-family: Arial, Helvetica, sans-serif;
      font-size: 14px; margin-top: 10px;
    }
    ul {
      margin: 0;
    }
    #contentWrap {
      width: 700px; height: auto;
      margin: 0 auto;
      overflow: hidden;
    }
    #contentTop {
      width: 600px; padding: 10px; margin-left: 30px;
    }
    #contentLeft {
      float: left; width: 400px;
    }
    #contentLeft li {
      list-style: none;
      margin: 0 0 4px 0; padding: 10px;
      background-color:#003399; color:#fff;
      border: #CCCCCC solid 1px;
    }
    #contentRight {
      float: right; width: 260px;
      padding:10px;
      background-color:#336600; color:#FFFFFF;
    }
  </style>
    
  <script type="text/javascript">
    $(document).ready(function() {
      $("#contentLeft ul").sortable({
        opacity: 0.5,
        cursor: "move",
        update: function() {
          var sorrend = $(this).sortable("serialize") +
                        "&action=listaFrissites";
          $.post("updateDB.php", sorrend, function(valasz) {
            $("#contentRight").html(valasz);
          }); 
        }
      });
    });
  </script>

</head>
<body>

  <div id="contentWrap">

    <div id="contentTop">
      <p>Tegye sorrendbe a kedvenc madarait.</p>
      <p></p>
    </div>
  
    <div id="contentLeft">
      <ul>
        <?php
          include_once("connectDB.php");
          $sql = "
            select *
            from draganddrop
            order by sorrend;
          ";
          $sorok = $conn->query($sql);
          while ($sor = $sorok->fetch_array(MYSQLI_ASSOC)) {
            $id = $sor["id"];
            $sz = $sor["szoveg"];
            echo '<li id="rekordTomb_'.$id.'">'.$sz.'</li>';
          }
        ?>
      </ul>
    </div>
    
    <div id="contentRight">
      <p>Itt jelenítjük majd meg az ellenőrző adatokat.</p>
      <p>&nbsp; </p>

    </div>
  
  </div>
  
  <a href="templates/pages/madar.php"><button>Ha itt nem megy klikk ide</button></a>

</body>
</html>
