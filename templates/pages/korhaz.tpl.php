<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <script type="text/javascript" src="jq.js"></script>
  <style type="text/css">
    div.doboz {
      width: 800px;
      margin-bottom: 10px;
    }
    div.dobozfej {
      padding: 2px 5px;
      border: 1px solid #666666;
      background-color: #cccccc;
    }
    div.doboztorzs {
      padding: 2px 5px;
      border: 1px solid #666666;
      border-top: 0;
      background-color: #eeeeee;
      height: 160px;
      overflow: auto;
    }
  </style>
  <script type="text/javascript">
    function dobozKiBe() {
      var id  = $(this).attr("id");
      var ssz = id.substr(3);
      $("#torzs" + ssz).slideToggle();
      if ($("#torzs" + ssz).css("display") == "none") {
        $(this).attr({
          src: "images/block-close.gif",
          title: "részletek elrejtése"
        });
      } else {
        $(this).attr({
          src: "images/block-open.gif",
          title: "részletek megjelenítése"
        });
      }
    }
    $(document).ready(function() {
      $(".doboztorzs").css("display", "none");
      $(".kep").attr({
        src: "images/block-open.gif",
        title: "részletek megjelenítése"
      }).css("cursor", "pointer");
      $(".kep").click(dobozKiBe);
    });
  </script>
</head>
<body>
  <div class="doboz" id="doboz1">
    <div class="dobozfej" id="fej1">
      <img src="images/block-close.gif" class="kep" id="kep1" title="nem működik">
      <span class="cim">Történet</span>
    </div>
    <div class="doboztorzs" id="torzs1">
     <p><img id="favicon" src="./images/favicon.ico">A Vadmadárkórház története 1996-ban kezdődött. Az állatszerető emberek sérült, beteg madarakat hoztak a székesfehérvári állatkórházba, segítséget kértek a madarak meggyógyításához. Ezt a feladatot Dr. Berkényi Tamás állatkórházi állatorvos vállalta fel, majd később megalapította az önálló székesfehérvári Vadmadárkórházat, melynek egyértelmű célja a madarak gyógyítása lett.</p>

<p><img id="favicon" src="./images/favicon.ico">Dr.Berkényi Tamás a madarak gyógyítására specializálódott, megszerezte az egzotikus állatok szakállatorvosa képesítést is. Igy jött létre az önkéntes alapon, támogatásokból, az erre áldozni hajlandó madárbarátok adományaiból működő Vadmadárkórház.</p>

<p><img id="favicon" src="./images/favicon.ico">Az elmúlt évek során a Vadmadárkórháznak híre ment, így mind több és több madár érkezett a kórházba, melyek gyógyítása, gondozása, tartása, etetése egyre nagyobb feladatot jelent. Ennek megoldásában a HEROSZ állatotthon segít. A megnövekedett betegforgalom jobb ellátása érdekében 2014-ben megnyílt a Vadmadárkórház második mentőközpontja Sukorón.</p>
	
	 
    </div>
  </div>
  <div class="doboz" id="doboz2">
    <div class="dobozfej" id="fej2">
      <img src="images/block-close.gif" class="kep" id="kep2" title="nem működik">
      <span class="cim">Gyógyítás</span>
    </div>
    <div class="doboztorzs" id="torzs2">
      
	  

<p><img id="favicon" src="./images/favicon.ico">A sérült vadmadarak egy része a gyógyulás után, maradandó károsodásuk miatt, soha nem térhet vissza a természetbe, nem engedhetők szabadon, mert a gyógyulásuk után visszamaradó maradandó károsodások nem teszik lehetővé számukra a zsákmányszerzést, menekülést, az önálló életet. Az ilyen madarak egész életükön át emberi gondoskodásra szorulnak. A Vadmadárkórház röpdéiben sokféle madárfaj él, mint pld. macskabagoly, fülesbagoly, uhu, pusztai sas, vörös vércse, fehér gólya, egerészölyv, és sok más hazai és átvonuló madár.</p>

<p><img id="favicon" src="./images/favicon.ico">A Vadmadárkórház kizárólag magánadományokból működik. Ebből fedezzük a fenntartását, gyógyszerek megvásárlását, az állatok ellátását. Ha a Vadmadárkórház munkáját hasznosnak tartja, és lehetősége van rá, kérjük segítse a madarak gyógyítását.</p>
 



    </div>
  </div>
</body>
</html>

