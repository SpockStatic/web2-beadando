<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		
<?php
	//Ellenőriz
	if(!isset($_POST["nev"]) || strlen($_POST["nev"]) < 5)
	{
		exit("Hibás név: ".$_POST['nev']);
	}

	$re = '/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/';
	if(!isset($_POST["email"]) || !preg_match($re,$_POST["email"]))
	{
		exit("Hibás email: ".$_POST["email"]);
	}

	if(!isset($_POST["szoveg"]) || empty($_POST["szoveg"]))
	{
		exit("Hibás szöveg: ".$_POST["szoveg"]);
	}

	try {
        // Kapcsolódás
        $dbh = new PDO('mysql:host=localhost;dbname=labor', 'root', '',
                        array(PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION));
        $dbh->query('SET NAMES utf8 COLLATE utf8_hungarian_ci');
        
            $sqlInsert = "insert into komment(nev, email, szoveg)
                          values(:nev, :email, :szoveg)";
            $stmt = $dbh->prepare($sqlInsert); 
            $stmt->execute(array(':nev' => $_POST['nev'], ':email' => $_POST['email'],
                                 ':szoveg' => $_POST['szoveg'])); 
           
        }
    
    catch (PDOException $e) {
        $uzenet = "Hiba: ".$e->getMessage();
        $ujra = true;
    }      

	echo "Név: ", $_POST['nev'];
	echo "</br>";
	echo "E-mail: ", $_POST["email"];
	echo "</br>";
	echo "Üzenet: ", $_POST["szoveg"];

?>
	</body>
</html>
