-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Hoszt: 127.0.0.1
-- Létrehozás ideje: 2015. Ápr 19. 19:20
-- Szerver verzió: 5.5.32
-- PHP verzió: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `web2_1617ii_gyak11b`
--
CREATE DATABASE IF NOT EXISTS `web2_1617ii_gyak11b` DEFAULT CHARACTER SET utf8 COLLATE utf8_hungarian_ci;
USE `web2_1617ii_gyak11b`;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `draganddrop`
--

CREATE TABLE IF NOT EXISTS `draganddrop` (
  `id` int(11) NOT NULL,
  `szoveg` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `draganddrop`
--

INSERT INTO `draganddrop` (`id`, `szoveg`, `sorrend`) VALUES
(1, 'Fecske', 4),
(2, 'Veréb', 2),
(3, 'Hullám papagáj', 3),
(4, 'Hollo', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
