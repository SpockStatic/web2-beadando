CREATE DATABASE IF NOT EXISTS `labor5`
CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `labor`;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  `lname` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=4 ;

INSERT INTO `users` (`id`, `fname`, `lname`) VALUES
('pl', 'PL');

CREATE TABLE IF NOT EXISTS `minusz` (
  `minusz` varchar(3) COLLATE utf8_hungarian_ci NOT NULL,
  `szorzo` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  `id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

INSERT INTO `minusz` (`id`, `minusz`, `szorzo`) VALUES
('2', '1.5', 1);